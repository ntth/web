<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getIndex(){
    	return view('pages.trang-chu');
    }
    public function getToiUuKenh(){
    	return view('pages.toi-uu-kenh');
    }
    public function getChayQuangCao(){
    	return view('pages.chay-quang-cao');
    }
    public function getSanXuatNoiDung(){
    	return view('pages.san-xuat-noi-dung');
    }
    public function getThongTinTaiKhoan(){
    	return view('pages.thong-tin-tai-khoan');
    }
    public function getLichSuGiaoDich(){
    	return view('pages.lich-su-giao-dich');
    }
    public function getThongBaoMoi(){
    	return view('pages.thong-bao-moi');
    }
    public function getHuongDanMuaHang(){
    	return view('pages.huong-dan-mua-hang');
    }
    public function getDieuKhoan(){
    	return view('pages.dieu-khoan');
    }
    public function getLienHe(){
    	return view('pages.lien-he');
    }
    public function getDoiMatKhau(){
    	return view('pages.doi-mat-khau');
    }
}
