
@extends('layouts.layout')
@section('NoiDung')
<div id="row"> <!-- Row-->
	<div class="col-md-6 box" style="margin-left: 25%;"> <!-- cot 1 -->
		<div id="box7" class="cardbox">
			<div class="card-header">
				<h4 id="h4-tittle">Đổi mật khẩu</h4>
				<p style="text-align: center;">Vui lòng điền thông tin liên hệ tới Admin.</p>
			</div>
			<div class="card-body">
				<div>
					<form id="form-change" action="doi-mat-khau.html" method="post" role="form">
						<input type="hidden" name="_csrf" value="">
						<div class="form-group field-changepasswordconfirmemail-email required bmd-form-group">
							<label class="control-label bmd-label-static" for="changepasswordconfirmemail-email">Email</label>
							<input type="text" id="change-pass" class="form-control" name="ChangePasswordConfirmEmail[email]" placeholder="Nhập địa chỉ email đã đăng ký tài khoản để nhận link đổi mật khẩu">
							<span id="error-change-pass" class="error"></span>
						</div>                                
						<div class="form-group">
							<button type="submit" class="btn btn-primary" name="change-button" onclick="changePass(); return false;">Submit</button>                          
						</div>
					</form>                            
				</div>
			</div>
		</div>
	</div> <!-- End cột 3 -->
</div><!-- End Row -->
@endsection