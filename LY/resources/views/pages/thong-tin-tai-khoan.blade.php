@extends('layouts.layout')
@section('NoiDung')
	<div id="row"> <!-- Row-->
		<div class="col-md-6 box" style="margin-left: 25%;"> <!-- cot 1 -->
			<div id="box4" class="cardbox">
				<div class="card-header">
					<h4 id="h4-tittle">Thông tin tài khoản</h4>
				</div>
				<div class="card-body">
					<form id="user-form" name="myForm" action="" method="post" enctype="multipart/form-data"> <!-- Biểu mẫu -->
						<input type="hidden" name="_csrf" value="">
						<div class="fform">
							<label for="favatar">Avatar</label><br>
							<div>
								<div>
									<img id="avatar_img" src="Images/1.png" alt="avatar">
									<input type="file" name="chooseAvatar">
									<!-- 
									<button class="btn btn-danger" type="file">Chọn ảnh đại diện</button> -->
								</div>
							</div>
						</div><!--  End Avatar -->
						<div class="fform">
							<label>Họ tên:</label>
							<input type="text" name="fullname" id="fullname" placeholder="Họ và tên">
							<span id="error-name" class="error"></span>
						</div> <!-- End Tên -->
						<div class="fform">
							<label>Điện thoại:</label>
							<input type="text" name="phone" id="phone" placeholder="Điện thoại">
							<span id="error-phone" class="error"></span>
						</div> <!-- Số đt -->
						<div class="fform">
							<label>Email:</label>
							<input type="text" name="email" id="email" placeholder="Email">
							<span class= "error-email error"></span>
						</div><!-- Email -->
						<div class="fform">
							<label>Địa chỉ:</label>
							<input type="text" name="faddress" id="address" placeholder="Địa chỉ">
							<span id="error-address" class="error"></span>
						</div> <!-- Địa chỉ -->
						<div class="fform">
							<label>Facebook:</label>
							<input type="text" name="facebook" id="facebook" placeholder="Facebook">
							<span id="error-facebook" class="error"></span>
						</div> <!-- Facebook -->
						<div class="fform" style="">
							<button id="cap-nhat" class="btn btn-danger" type="submit" style="margin-top: 2em;" onclick="userInformation(); return false;">Cập nhật</button>
							<!-- <input id="cap-nhat" type="submit" name="Cập nhật"> -->
						</div>

					</form> <!-- End biểu mẫu -->
				</div>
			</div>
		</div> <!-- End cột 3 -->
	</div><!-- End Row -->
@endsection