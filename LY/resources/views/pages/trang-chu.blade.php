@extends('layouts.layout')
@section('NoiDung')
	<div id="row" style="margin-top: 5em;"> <!-- Row-->
		<div id="box1" class="box col-lg-4 col-md-6 col-sm-6"> <!-- Cột 1 -->
			<a class="cardbox" href="{{ asset('toi-uu-kenh') }}">
				<div class="cardbox-header">
					<img src="{{ asset('images/YouTube-icon.png') }}" alt="logo-youtube" style="width: 60px;">
					<p class="cardbox-category">0 sản phẩm</p>
					<h3 class="h-tittle">TỐI ƯU KÊNH</h3>
				</div>
				<div class="cardbox-footer">
					<div class="stats">
						<p class="cardbox-p">Xem chi tiết
							<i class="fas fa-angle-right"></i>
						</p>
					</div>
				</div>
			</a>
		</div> <!-- End cột 1 -->
		<div id="box2" class="box col-lg-4 col-md-6 col-sm-6"> <!-- Cột 2 -->
			<a class="cardbox" href="{{ asset('chay-quang-cao') }}">
				<div class="cardbox-header">
					<img src="{{ asset('images/YouTube-icon.png') }}" alt="logo-youtube" style="width: 60px;">
					<p class="cardbox-category">0 sản phẩm</p>
					<h3 class="h-tittle">CHẠY QUẢNG CÁO</h3>
				</div>
				<div class="cardbox-footer">
					<div class="stats">
						<p class="cardbox-p">Xem chi tiết
							<i class="fas fa-angle-right"></i>
						</p>
					</div>
				</div>
			</a>
		</div> <!-- End cột 2 -->
		<div id="box3" class="box col-lg-4 col-md-6 col-sm-6"> <!-- cột 3 -->
			<a class="cardbox" href="{{ asset('san-xuat-noi-dung') }}">
				<div class="cardbox-header">
					<img src="{{ asset('images/YouTube-icon.png') }}" alt="logo-youtube" style="width: 60px;">
					<p class="card-category">0 sản phẩm</p>
					<h3 class="h-tittle">SẢN XUẤT NỘI DUNG</h3>
				</div>
				<div class="cardbox-footer">
					<div class="stats">
						<p class="cardbox-p"align="">Xem chi tiết
							<i class="fas fa-angle-right"></i>
						</p>
					</div>
				</div>
			</a>
		</div> <!-- End cột 3 -->
	</div><!-- End Row -->
@endsection