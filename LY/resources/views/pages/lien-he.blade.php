@extends('layouts.layout')
@section('NoiDung')
	<div id="row"> <!-- Row-->
		<div class="col-md-6 box" style="margin-left: 25%;"> <!-- cot 1 -->
			<div id="box7" class="cardbox">
				<div class="card-header">
					<h4 id="h4-tittle">Liên hệ</h4>
					<p style="text-align: center;">Vui lòng điền thông tin liên hệ tới Admin.</p>
				</div>
				<div class="card-body">
					<form id="contact-form" action="#" method="post" role="form">
						<div class="fform">
							<input type="text" name="ct-fullname" id="ct-fullname" placeholder="Họ và tên">
							<span id="ct-error-name" class="error"></span>
						</div> <!-- Tên -->
						<div class="fform">
							<input type="text" name="ct-phone" id="ct-phone" placeholder="Điện thoại">
						</div> <!-- Số đt -->
						<div class="fform">
							<input type="text" name="ct-email" id="ct-email" placeholder="Email">
						</div><!-- Email -->
						<div class="fform">
							<input type="text" name="ct-faddress" id="ct-address" placeholder="Địa chỉ">
						</div> <!-- Địa chỉ -->
						<div class="fform">
							<input type="text" name="ct-facebook" id="ct-facebook" placeholder="Facebook">
						</div> <!-- Facebook -->
						<div class="fform" style="">
							<button id="gui-yeu-cau" class="btn btn-danger" type="submit" style="margin-top: 2em;" onclick="contact(); return false;">Gửi yêu cầu</button>
							<!-- <input id="cap-nhat" type="submit" name="Cập nhật"> -->
						</div>
					</form>
				</div>
			</div>
		</div> <!-- End cột 3 -->
	</div><!-- End Row -->
@endsection