<div id="menu"> <!-- menu sidebar -->
	<div id="logo">
		<a href="{{ asset('trang-chu') }}"><i class="fas fa-home"></i>&nbsp;&nbsp;&nbsp;TRANG CHỦ</a>
	</div>
	<div id="background-menu">
		<div id ="card"> <!-- card -->
			<div class="card-avatar">
				<a href="{{ asset('trang-chu') }}">
					<img src="{{ asset('images/1.png') }}" alt="card-avatar">
				</a>
			</div>
			<div class="card-body">
				<h4 class="card-tittle">nhom8@yt</h4>
				<div class="money-money">
					<div class="total">
						<label>$0,00</label>
					</div>
					<div class="rest">
						<label>$0,00</label>
					</div>
				</div>
			</div>
		</div>  <!-- End card -->
		<div id="sub-menu"> <!-- danh sách dịch vụ -->
			<ul>
				<li><a href="{{ asset('trang-chu') }}" class="div-icon"><i class="fab fa-servicestack"></i>&nbsp;&nbsp;&nbsp;Dịch Vụ</a></li>
				<li><a href="{{ asset('thong-tin-tai-khoan') }}" class="div-icon"><i class="fas fa-user-alt"></i>&nbsp;&nbsp;&nbsp;Tài Khoản</a></li>
				<li><a href="{{ asset('lich-su-giao-dich') }}" class="div-icon"><i class="fas fa-history"></i>&nbsp;&nbsp;&nbsp;Lịch Sử Giao Dịch</a></li>
				<li><a href="{{ asset('thong-bao-moi') }}" class="div-icon"><i class="far fa-bell"></i>&nbsp;&nbsp;&nbsp;Thông Báo Mới</a></li>
				<li>
					<a style="cursor: pointer" class="div-icon"><i class="fas fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;Hỗ Trợ</a>
					<ul class="sub-hotro">
						<li><a href="{{ asset('huong-dan-mua-hang') }}">Hướng Dẫn Mua Hàng</a></li>
						<li><a href="{{ asset('dieu-khoan') }}">Điều Khoản</a></li>
						<li><a href="{{ asset('lien-he') }}">Liên Hệ</a></li>
					</ul>
				</li>
			</ul>
		</div> <!-- End danh sách dịch vụ -->
	</div> <!-- End background menu -->
</div> <!-- End nội dung menu sidebar -->