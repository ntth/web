<nav id="navbar"> <!-- khối liên kết điều hướng -->
	<div id="navbar-collapse">
		<ul id="navbar-nav1">
			<li><a class="nav-link" href="tel:0999999999" style="font-size: 18px;">HOTLINE:0999999999</a></li>
			<li><a class="nav-link" href="https://facebook.com"><i class="fab fa-facebook"></i></a></li>
			<li><a class="nav-link" href="https://telegram.com"><i class="fab fa-telegram-plane"></i></a></li>
			<li><a class="nav-link" href="https://youtube.com"><i class="fab fa-youtube"></i></a></li>
			<li><a class="nav-link" href="mailto:admin@"><i class="far fa-envelope"></i></a></li>
		</ul>
		<ul id="navbar-nav2">
			<li id="li2">
				<a id="a-click" class="nav-link" href="{{ asset('thong-bao-moi') }}"><i class="fas fa-bell"></i></a>
				<!-- <div id="noti-click" class="dropdown_menu box dropdown-menu-right">
					<p style="color: black;">Thông báo</p>
				</div> -->
			</li>
			<li id="li2">
				<a id="a-click" style="cursor: pointer" class="nav-link" onclick="userClick()"><i class="fas fa-user-alt"></i></i></a>
				<div id="acc-click" class="dropdown_menu box dropdown-menu-right">
					<a class="dropdown-item" href="{{ asset('doi-mat-khau') }}">Đổi mật khẩu</a>
					<a class="dropdown-item" href="#">Đăng xuất</a>
				</div>
			</li>
		</ul>
	</div>
</nav><!-- End khối liên kết điều hướng -->